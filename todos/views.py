from django.shortcuts import (
    render,
    get_object_or_404,
    redirect,
)
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, ItemForm


# Create your views here.
def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/myLists.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo_list,
    }
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", form.instance.id)
    else:
        form = TodoListForm
    context = {
        "form": form,
    }
    return render(request, "todos/create_list.html", context)


def update_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)
    context = {
        "todo_object": todo_list,
        "form": form,
    }
    return render(request, "todos/update_list.html", context)


def delete_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")

    context = {
        "todo_list": todo_list,
    }

    return render(request, "todos/delete.html", context)


def create_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def edit_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = ItemForm(instance=todo_item)
    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)
